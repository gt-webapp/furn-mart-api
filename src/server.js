const http = require("http");

const app = require("./app");

const port = process.env.PORT || 8095; 

const server = http.createServer(app);


server.listen(port ,() => console.log(`listening at port ${port}` ));

