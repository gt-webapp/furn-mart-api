const express = require("express");
const cors = require("cors");
var multipart = require('connect-multiparty');


const Funmart_Selling_Api = require("./api/FunMartSelling")

const app = express();
app.use(express.json({limit: '50MB', extended: true}));
app.use(express.urlencoded({limit: '50MB', extended: true }));
app.use(cors());
app.use(multipart({
    maxFieldsSize: '50MB'
}));


app.use("/FunMart", Funmart_Selling_Api)

module.exports = app;